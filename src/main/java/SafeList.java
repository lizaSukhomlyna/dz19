public interface SafeList<E> {
    boolean add(E element);
    boolean remove(int index);
    E get(int index);
}
