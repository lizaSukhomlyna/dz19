import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class PetrolStation {
    private double amount = 700;//количество топлива на станции
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();

    public double doRefuel(float givePetrol) {
        writeLock.lock();
        try {
            this.amount = this.amount - givePetrol;
            return amount;
        } finally {
            writeLock.unlock();
        }
    }

    public double getAmount() {
        readLock.lock();
        try {
            return amount;
        } finally {
            readLock.unlock();
        }
    }
}
