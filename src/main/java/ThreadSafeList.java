import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class ThreadSafeList<T> implements SafeList<T> {

    private T[] safeList;
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final Lock readLock = readWriteLock.readLock();
    private final Lock writeLock = readWriteLock.writeLock();

    public ThreadSafeList() {
        this.safeList = (T[]) new Object[1];
    }

    public Object getSafeList() {
        readLock.lock();
        try {
            return safeList;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public boolean add(T element) {
        writeLock.lock();
        try {
            T[] es = this.safeList;
            int len = es.length;
            if (len == 1 && (es[len - 1] == null)) {
                es = Arrays.copyOf(this.safeList, len);
                es[len - 1] = element;
            } else {
                es = Arrays.copyOf(this.safeList, len + 1);
                es[len] = element;
            }
            this.safeList = es;
            return true;
        } finally {
            writeLock.unlock();
        }
    }

    public T get(int index) {
        readLock.lock();
        try {
            return this.safeList[index];
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public boolean remove(int index) {
        writeLock.lock();
        try {
            Object[] es = this.safeList;
            int len = this.safeList.length;
            Object oldValue = this.safeList[index];
            int numMoved = len - index - 1;
            Object[] newElements;
            if (numMoved == 0)
                newElements = Arrays.copyOf(es, len - 1);
            else {
                newElements = new Object[len - 1];
                System.arraycopy(es, 0, newElements, 0, index);
                System.arraycopy(es, index + 1, newElements, index,
                        numMoved);
            }
            this.safeList = (T[]) newElements;
            return true;
        } finally {
            writeLock.unlock();
        }

    }

    @Override
    public String toString() {
        readLock.lock();
        try {
            return "ThreadSafeList{" +
                    "safeList=" + Arrays.toString(safeList) +
                    '}';
        } finally {
            readLock.unlock();
        }
    }
}
