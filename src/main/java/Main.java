import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Task#0");

        ThreadSafeList<Integer> safeInt = new ThreadSafeList<>();
        safeInt.add(1);
        safeInt.add(2);
        safeInt.add(3);
        System.out.println(safeInt.get(0));
        System.out.println(safeInt.toString());
        safeInt.get(0);
        safeInt.remove(0);
        System.out.println(safeInt.toString());

        System.out.println("Task#1");
        Semaphore sem = new Semaphore(3);
        PetrolStation petrolStation = new PetrolStation();
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);
        executorService.scheduleWithFixedDelay(new OurTask(sem, petrolStation), 0, 3, TimeUnit.SECONDS);

    }
}

