import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;

public class OurTask implements Runnable {
    private Semaphore sem;
    private PetrolStation petrolStation;


    public OurTask(Semaphore sem, PetrolStation petrolStation) {
        this.sem=sem;
        this.petrolStation = petrolStation;
    }

    @Override
    public void run(){
        try {
            System.out.println(Thread.currentThread().getName()) ;
            sem.acquire();
            petrolStation.doRefuel(15);
            System.out.println(petrolStation.getAmount());
            Thread.sleep(1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            sem.release();
        }
        sem.release();
    }

}
